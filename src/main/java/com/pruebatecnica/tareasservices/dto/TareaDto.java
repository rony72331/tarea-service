package com.pruebatecnica.tareasservices.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TareaDto {
    private String nombre;
    private String descripcion;
    private Date fecha_entrega;
    private List<String> usernamesAsignados;

}
