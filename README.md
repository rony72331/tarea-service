# Tareas Services
Este proyecto proporciona un servicio para gestionar usuarios y tareas asociadas. Cada usuario tiene un nombre de usuario único y una lista de tareas. Cada tarea tiene un título, una descripción y un estado (pendiente o completada). El servicio es implementado utilizando Spring Boot y se expone a través de una API RESTful.

## Endpoints
 ### Usuarios
 - GET /api/getUsuarios: Obtiene la lista de todos los usuarios.
 - GET /api/getUsuarioByUsername/{username}: Obtiene un usuario por su nombre de usuario.
 - POST /api/saveUsuario: Crea un nuevo usuario.
 - POST /api/addUsuarioTarea: Asocia un usuario existente a una tarea existente.
### Tareas
- GET /api/getTareas: Obtiene la lista de todas las tareas.
- GET /api/getTareaByCodigo/{codigo}: Obtiene una tarea por su código.
- POST /api/saveTarea: Crea una nueva tarea.
- POST /api/terminarTarea: Marca una tarea como completada.

## Ejecucion
dentro del proyecto en la siguiente ruta:

main/target/tareas-service-0.0.1-SNAPSHOT.jar

abrir ventana de cmd y ejecutar el siguiente comando

java -jar {ruta raiz del proyecto}\target\tareas-service-0.0.1-SNAPSHOT.jar

Puede acceder a la documentacion mas detallada en el swagger
### http://localhost:8080/swagger-ui/index.html#/

Dependencias
Spring Boot 2.5.0
Spring Data JPA
Hibernate
Argon2